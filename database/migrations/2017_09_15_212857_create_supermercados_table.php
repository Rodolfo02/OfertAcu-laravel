<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSupermercadosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('supermercados', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nome');
            $table->string('endereco');
            $table->string('photo');
            $table->bigInteger('CNPJ')->unique();
            $table->text('razao_social');
            $table->string('email', '100')->unique();
            $table->decimal('telefone')->unique();
            $table->decimal('latitude', '11', '7');
            $table->decimal('longitude', '11', '7');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('supermercados');
    }
}
