<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProdutosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('produtos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nome');
            $table->string('descricao');
            $table->string('preco');
            $table->string('photo');
            $table->date('data_inserção');
            $table->integer('marca_id')->unsigned();
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::table('produtos', function (Blueprint $table){
           $table->foreign('marca_id')
               ->references('id')
               ->on('marcas')
               ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('produtos');
    }
}
