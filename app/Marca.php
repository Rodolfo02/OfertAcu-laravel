<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Marca extends Model
{
    use SoftDeletes;
    protected $table = 'marcas';
    protected $fillable = ['nome'];
    protected $dates = ['deleted_at'];

    public function Produtos ()
    {
        return $this->hasMany(Produto::class);
    }

}
