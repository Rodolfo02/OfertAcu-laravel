<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Supermercado extends Model
{
    use SoftDeletes;
    protected $table = 'supermercados';
    protected $fillable = ['nome', 'endereco', 'photo', 'CNPJ', 'razao_social', 'email', 'telefone', 'latitude', 'longitude'];
    protected $dates = ['deleted_at'];

    public function Produtos()
    {
        return $this->belongsToMany(Supermercado::class);
    }
}
