<?php

namespace App\Http\Controllers;

use App\Marca;
use App\Produto;
use Illuminate\Http\Request;

class MarcaController extends Controller
{

    public function index()
    {
        $marcas = Marca::all();
        return response()->json($marcas);
    }

    public function show ($id)
    {

        $marca = Marca::find($id);
        return response()->json($marca);

    }

    public function create ()
    {

    }
    public function store ()
    {

    }

    public function destroy ()
    {

    }

    public function update ()
    {

    }

    public function edit ()
    {

    }

}
