<?php

namespace App\Http\Controllers;

use App\Produto;
use App\Marca;
use App\Supermercado;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProdutoController extends Controller
{

    public function index()
    {
        $produtos = Produto::all();
        return response()->json($produtos);
    }

    public function show ($id)
    {

    }

    public function create ()
    {

    }
    public function store ()
    {

    }

    public function destroy ()
    {

    }

    public function update ()
    {

    }

    public function edit ()
    {

    }
}
