<?php

namespace App\Http\Controllers;

use App\Produto;
use App\Supermercado;
use Illuminate\Http\Request;
use DB;

class SupermercadoController extends Controller
{
    public function index()
    {
        $supermercado = Supermercado::all();
        return response()->json($supermercado);
    }

    public function listarProd()
    {
        $supers = Supermercado::where('id', '1')->get();
        foreach ($supers as $super)
        {
            $produtos = $super->Produtos()->get();
            foreach ($produtos as $produto)
            {
                return response()->json([$super->nome, $produto->nome]);
            }
        }
    }

    public function show ($id)
    {

    }

    public function create ()
    {

    }
    public function store ()
    {

    }

    public function destroy ()
    {

    }

    public function update ()
    {

    }

    public function edit ()
    {

    }


}
