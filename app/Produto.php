<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Produto extends Model
{
    use SoftDeletes;
    protected $table = 'produtos';
    protected $fillable = ['none', 'descricao', 'preco', 'photo', 'data_insercao'];
    protected $dates = ['deleted_at'];

    public function Marcas()
    {
        return $this->belongsTo(Marca::class, 'marca_id', 'id');
    }

    public function Supermercados()
    {
        return $this->belongsToMany(Produto::class);
    }
}
